from django.db import models

class Users(models.Model):
    User_id = models.BigAutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.IntegerField()
    address = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name


class AllExpenses(models.Model):
    id = models.BigAutoField(primary_key=True)
    date_of_spent = models.DateField()
    category = models.CharField(max_length=100)
    particulars = models.CharField(max_length=100)
    type_of_spent = models.CharField(max_length=100)
    amount = models.IntegerField()
    user = models.ForeignKey(
        Users, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.category



