# Generated by Django 3.2.3 on 2021-12-23 08:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api_basic', '0004_rename_freequency_allexpenses_freequency_of_spent'),
    ]

    operations = [
        migrations.RenameField(
            model_name='allexpenses',
            old_name='freequency_of_spent',
            new_name='type_of_spent',
        ),
    ]
