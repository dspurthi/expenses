from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import Users,AllExpenses



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['User_id','first_name','last_name','email','phone','address']

class AllExpensesSerializer(serializers.ModelSerializer):
    class Meta:
        model = AllExpenses
        fields = ['id','date_of_spent','category','particulars','type_of_spent','amount','user']