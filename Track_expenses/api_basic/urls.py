from django.contrib import admin
from django.urls import path
from .views import users_list,expenses_details

urlpatterns =[
    path('users/',users_list),
    path('expenses/',expenses_details)
]