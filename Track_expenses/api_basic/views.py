from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from rest_framework import serializers
from rest_framework.parsers import JSONParser
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from .models import AllExpenses, Users
from .serializers import UserSerializer,AllExpensesSerializer


# Creating users requests GET,POST
@csrf_exempt
def users_list(request):
    if request.method == 'GET':
        users = Users.objects.all()
        serializer = UserSerializer(users,many=True)
        return JsonResponse(serializer.data,safe= False)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors,status=400)

## creating expenses requests OF GET & POST

@csrf_exempt
def expenses_details(request):
    if request.method == 'GET':
        expenses = AllExpenses.objects.all()
        serializer = AllExpensesSerializer(expenses,many=True)
        return JsonResponse(serializer.data,safe= False)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = AllExpensesSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors,status=400)