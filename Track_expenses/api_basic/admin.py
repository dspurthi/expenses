from django.contrib import admin
from .models import Users,AllExpenses

# Register your models here.
admin.site.register(Users)
admin.site.register(AllExpenses)
